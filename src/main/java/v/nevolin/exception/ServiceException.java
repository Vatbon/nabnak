package v.nevolin.exception;

public class ServiceException extends RuntimeException {

    public ServiceException(String s) {
        super(s);
    }
}
