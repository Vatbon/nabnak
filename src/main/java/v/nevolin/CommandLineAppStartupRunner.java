package v.nevolin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import v.nevolin.model.db.*;
import v.nevolin.repository.*;
import v.nevolin.util.TimeUtil;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
    private static final Logger LOG =
            LoggerFactory.getLogger(CommandLineAppStartupRunner.class);

    private final UserRepository userRepository;
    private final DashboardRepository dashboardRepository;
    private final TaskRepository taskRepository;
    private final TagRepository tagRepository;
    private final CommentRepository commentRepository;
    private final LogRepository logRepository;
    private final DashboardRoleRepository dashboardRoleRepository;


    @Autowired
    public CommandLineAppStartupRunner(UserRepository userRepository,
                                       DashboardRepository dashboardRepository,
                                       TaskRepository taskRepository,
                                       TagRepository tagRepository,
                                       CommentRepository commentRepository,
                                       LogRepository logRepository,
                                       DashboardRoleRepository dashboardRoleRepository) {
        this.userRepository = userRepository;
        this.dashboardRepository = dashboardRepository;
        this.taskRepository = taskRepository;
        this.tagRepository = tagRepository;
        this.commentRepository = commentRepository;
        this.logRepository = logRepository;
        this.dashboardRoleRepository = dashboardRoleRepository;
    }

    @Override
    public void run(String... args) {
        initRepository();
        LOG.info("Initialized database with example");
    }

    private void initRepository() {
        User admin = new User(0L, "admin", "admin@email.net", "admin");
        User user = new User(0L, "user", "user@email.net", "user");
        admin = userRepository.addUser(admin);
        user = userRepository.addUser(user);

        Dashboard dashboard = new Dashboard();
        dashboard.setName("testDashboard");
        dashboard.setHash("testHash");
        dashboard = dashboardRepository.addDashboard(dashboard);

        DashboardRole dashboardRoleAdmin = new DashboardRole();
        dashboardRoleAdmin.setDashboardId(dashboard.getId());
        dashboardRoleAdmin.setRoleId(0L);
        dashboardRoleAdmin.setUserId(admin.getId());
        dashboardRoleRepository.addUserToDashboard(dashboardRoleAdmin);

        DashboardRole dashboardRoleUser = new DashboardRole();
        dashboardRoleUser.setDashboardId(dashboard.getId());
        dashboardRoleUser.setRoleId(1L);
        dashboardRoleUser.setUserId(user.getId());
        dashboardRoleRepository.addUserToDashboard(dashboardRoleUser);

        Task inProgressTask = new Task();
        inProgressTask.setDashboardId(dashboard.getId());
        inProgressTask.setName("doing stuff");
        inProgressTask.setDescription("we need to complete this task asap");
        inProgressTask.setCreatedByUserId(admin.getId());
        inProgressTask.setAssignedToUserId(user.getId());
        inProgressTask.setStateId(1L);
        inProgressTask.setCreatedTime(TimeUtil.getCurrentTime());
        inProgressTask.setLastUpdatedTime(TimeUtil.getCurrentTime());
        taskRepository.addTask(inProgressTask);

        Task doneTask = new Task();
        doneTask.setDashboardId(dashboard.getId());
        doneTask.setName("done stuff");
        doneTask.setDescription("we have done this task");
        doneTask.setCreatedByUserId(admin.getId());
        doneTask.setAssignedToUserId(user.getId());
        doneTask.setStateId(2L);
        doneTask.setCreatedTime(TimeUtil.getCurrentTime());
        doneTask.setLastUpdatedTime(TimeUtil.getCurrentTime());
        doneTask = taskRepository.addTask(doneTask);

        Tag tag1 = new Tag();
        tag1.setName("Urgent");
        tag1.setTaskId(doneTask.getId());
        tagRepository.addTag(tag1);
        Tag tag2 = new Tag();
        tag2.setName("Extreme");
        tag2.setTaskId(doneTask.getId());
        tagRepository.addTag(tag2);

        Comment comment = new Comment();
        comment.setAuthorUserId(user.getId());
        comment.setCreatedTime(TimeUtil.getCurrentTime());
        comment.setTaskId(doneTask.getId());
        comment.setBody("you can count on me");
        commentRepository.addComment(comment);

        Comment comment1 = new Comment();
        comment1.setAuthorUserId(admin.getId());
        comment1.setCreatedTime(TimeUtil.getCurrentTime());
        comment1.setTaskId(doneTask.getId());
        comment1.setBody("nicely done");
        commentRepository.addComment(comment1);

        Log log = new Log();
        log.setAuthorUserId(user.getId());
        log.setTaskId(doneTask.getId());
        log.setComment("i've done smth. now it works");
        log.setCreatedTime(TimeUtil.getCurrentTime());
        log.setTime(15);
        logRepository.addLog(log);
    }
}
