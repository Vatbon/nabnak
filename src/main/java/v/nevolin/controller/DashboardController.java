package v.nevolin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import v.nevolin.model.api.DashboardDto;
import v.nevolin.model.db.Dashboard;
import v.nevolin.model.db.DashboardRole;
import v.nevolin.model.db.User;
import v.nevolin.repository.DashboardRepository;
import v.nevolin.service.DashboardService;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
public class DashboardController {
    private final DashboardService dashboardService;
    private final DashboardRepository dashboardRepository;

    @Autowired
    public DashboardController(DashboardService dashboardService, DashboardRepository dashboardRepository) {
        this.dashboardService = dashboardService;
        this.dashboardRepository = dashboardRepository;
    }

    @PostMapping("/api/v1/dashboard/create")
    public Dashboard createDashboard(@RequestBody Dashboard dashboard, @PathParam("userId") Long userId) {
        return dashboardService.createDashboard(dashboard, userId);
    }

    @GetMapping("/api/v1/dashboard/entity")
    public Dashboard getDashboardhash(@PathParam("id") Long id) {
        return dashboardRepository.getDashboardById(id);
    }

    @GetMapping("/api/v1/dashboards")
    public List<DashboardRole> getUsersDashboards(@PathParam("userId") Long userId) {
        return dashboardService.getUsersDashboards(userId);
    }

    @GetMapping("/api/v1/dashboard")
    public DashboardDto getFullDashboard(@PathParam("hash") String hash) {
        return dashboardService.buildDashboardByHash(hash);
    }

    @PutMapping("/api/v1/dashboard/join")
    public ResponseEntity joinDashboard(@PathParam("hash") String hash, @RequestBody User user) {
        dashboardService.joinDashboard(hash, user.getId());
        return ResponseEntity.ok().build();
    }

    @PutMapping("/api/v1/dashboard/leave")
    public ResponseEntity leaveDashboard(@PathParam("hash") String hash, @RequestBody User user) {
        dashboardService.leaveDashboard(hash, user.getId());
        return ResponseEntity.ok().build();
    }

    @PutMapping("/api/v1/dashboard/changerole")
    public DashboardRole changeRole(@PathParam("hash") String hash,
                                    @PathParam("adminId") Long adminId,
                                    @PathParam("userId") Long userId,
                                    @PathParam("roleId") Long roleId) {
        return dashboardService.changeRole(hash, adminId, userId, roleId);
    }
}
