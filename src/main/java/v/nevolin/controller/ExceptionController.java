package v.nevolin.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import v.nevolin.exception.ServiceException;

@ControllerAdvice
public class ExceptionController {
    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<String> httpError(ServiceException serviceException) {
        return ResponseEntity.status(403).body(serviceException.getMessage());
    }
}
