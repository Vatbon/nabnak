package v.nevolin.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GeneralController {
    @GetMapping
    public ResponseEntity checkConnection() {
        return ResponseEntity.ok().build();
    }
}
