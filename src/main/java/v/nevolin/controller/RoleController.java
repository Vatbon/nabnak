package v.nevolin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import v.nevolin.model.db.Role;
import v.nevolin.repository.RoleRepository;

import java.util.List;

@RestController
public class RoleController {
    private final RoleRepository roleRepository;

    @Autowired
    public RoleController(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @GetMapping("/api/v1/roles")
    public List<Role> getRoles() {
        return roleRepository.getAllRoles();
    }
}
