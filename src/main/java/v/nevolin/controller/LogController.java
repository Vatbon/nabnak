package v.nevolin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import v.nevolin.model.db.Log;
import v.nevolin.service.LogService;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
public class LogController {

    private final LogService logService;

    @Autowired
    public LogController(LogService logService) {
        this.logService = logService;
    }

    @GetMapping("/api/v1/log")
    public List<Log> getLogsByTaskId(@PathParam("taskId") Long taskId) {
        return logService.getLogsByTaskId(taskId);
    }

    @PostMapping("/api/v1/log/create")
    public Log createLog(@RequestBody Log log) {
        return logService.createLog(log);
    }
}
