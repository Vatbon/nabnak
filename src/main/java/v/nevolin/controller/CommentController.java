package v.nevolin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import v.nevolin.model.db.Comment;
import v.nevolin.service.CommentService;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/api/v1/comment/create")
    public Comment createComment(@RequestBody Comment comment) {
        return commentService.createComment(comment);
    }

    @PutMapping("api/v1/comment/update")
    public Comment updateComment(@RequestBody Comment comment) {
        return commentService.updateComment(comment);
    }

    @GetMapping("api/v1/comment")
    public Comment getCommentById(@PathParam("commentId") Long commentId) {
        return commentService.getCommentById(commentId);
    }

    @GetMapping("api/v1/comment/task")
    public List<Comment> getCommentsByTaskId(@PathParam("taskId") Long taskId) {
        return commentService.getCommentsByTaskId(taskId);
    }
}
