package v.nevolin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import v.nevolin.model.api.UserDto;
import v.nevolin.model.db.User;
import v.nevolin.service.UserService;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PutMapping("/api/v1/login")
    public User logIn(@RequestBody User user) {
        return userService.logIn(user);
    }

    @PostMapping("/api/v1/register")
    public User registerUser(@RequestBody User user) {
        return userService.registerUser(user);
    }

    @GetMapping("/api/v1/user")
    public User getUserById(@PathParam("id") Long id) {
        return userService.getUserById(id);
    }

    @GetMapping("/api/v1/user/dashboard")
    public List<UserDto> getUserByDashboardHash(@PathParam("hash") String hash) {
        return userService.getUsersByHash(hash);
    }

}
