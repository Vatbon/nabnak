package v.nevolin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import v.nevolin.model.api.FTSDto;
import v.nevolin.model.db.Comment;
import v.nevolin.model.db.Log;
import v.nevolin.model.db.Task;
import v.nevolin.service.FullTextService;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
public class FullTextController {

    private final FullTextService fullTextService;

    @Autowired
    public FullTextController(FullTextService fullTextService) {
        this.fullTextService = fullTextService;
    }

    @PutMapping("/api/v1/fts/comments")
    public List<Comment> fullTextSearchComments(@RequestBody FTSDto ftsDto, @PathParam("dashboardId") Long dashboardId) {
        return fullTextService.findComments(ftsDto.getText(), ftsDto.getUserId(), dashboardId);
    }

    @PutMapping("/api/v1/fts/logs")
    public List<Log> fullTextSearchLogs(@RequestBody FTSDto ftsDto, @PathParam("dashboardId") Long dashboardId) {
        return fullTextService.findLogs(ftsDto.getText(), ftsDto.getUserId(), dashboardId);
    }

    @PutMapping("/api/v1/fts/tasks")
    public List<Task> fullTextSearchTasks(@RequestBody FTSDto ftsDto, @PathParam("dashboardId") Long dashboardId) {
        return fullTextService.findTasks(ftsDto.getText(), ftsDto.getUserId(), dashboardId);
    }
}
