package v.nevolin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import v.nevolin.model.api.TaskDto;
import v.nevolin.model.db.Task;
import v.nevolin.service.TaskService;

import javax.websocket.server.PathParam;

@RestController
public class TaskController {
    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PutMapping("/api/v1/task")
    public TaskDto createTask(@RequestBody Task task) {
        return taskService.createTask(task);
    }

    @PutMapping("/api/v1/task/changestate")
    public TaskDto changeState(@PathParam("taskId") Long taskId, @PathParam("stateId") Long stateId) {
        return taskService.changeStateOfTask(taskId, stateId);
    }

    @PutMapping("/api/v1/task/assign")
    public TaskDto assignTask(@PathParam("taskId") Long taskId, @PathParam("userId") Long userId) {
        return taskService.assignTask(taskId, userId);
    }

    @PutMapping("/api/v1/task/update")
    public TaskDto updateTask(@RequestBody TaskDto taskDto) {
        return taskService.updateTask(taskDto);
    }
}
