package v.nevolin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import v.nevolin.model.db.Tag;
import v.nevolin.service.TagService;

@RestController
public class TagController {

    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @PutMapping("/api/v1/tag/add")
    public void addTag(@RequestBody Tag tag) {
        tagService.addTag(tag);
    }

    @PutMapping("/api/v1/tag/delete")
    public void deleteTag(@RequestBody Tag tag) {
        tagService.deleteTag(tag);
    }
}
