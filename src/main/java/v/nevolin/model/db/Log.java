package v.nevolin.model.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Log {
    private Long id;
    private Long authorUserId;
    private Long taskId;
    private String createdTime;
    private int time;
    private String comment;
}
