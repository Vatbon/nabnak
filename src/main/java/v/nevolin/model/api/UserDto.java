package v.nevolin.model.api;

import lombok.Data;
import v.nevolin.model.db.User;

@Data
public class UserDto {
    private User user;
    private Long roleId;
}
