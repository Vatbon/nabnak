package v.nevolin.model.api;

import lombok.Data;
import v.nevolin.model.db.Tag;
import v.nevolin.model.db.Task;

import java.util.List;

@Data
public class TaskDto {
    private Task task;
    private List<Tag> tagList;
}
