package v.nevolin.model.api;

import lombok.Data;
import v.nevolin.model.db.*;

import java.util.List;

@Data
public class DashboardDto {
    private Dashboard dashboard;
    private List<DashboardRole> dashboardRoleList;
    private List<TaskDto> taskList;
    private List<Comment> commentList;
    private List<Log> logList;
}
