package v.nevolin.repository;

import v.nevolin.model.db.Role;

import java.util.List;

public interface RoleRepository {
    List<Role> getAllRoles();

    Role getRoleById(Long id);
}
