package v.nevolin.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import v.nevolin.model.db.Log;
import v.nevolin.repository.extractor.LogExtractor;
import v.nevolin.repository.extractor.LogListExtractor;

import java.util.List;

@Repository("LogRepositoryImpl")
@DependsOn({"UserRepositoryImpl", "TaskRepositoryImpl"})
public class LogRepositoryImpl implements LogRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final LogListExtractor logListExtractor;
    private final LogExtractor logExtractor;

    @Autowired
    public LogRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        logListExtractor = new LogListExtractor();
        logExtractor = new LogExtractor();
        init();
    }

    private void init() {
        String sqlGroup = "CREATE TABLE IF NOT EXISTS LOGS(" +
                "LOG_ID IDENTITY PRIMARY KEY," +
                "AUTHOR_USER_ID LONG," +
                "TASK_ID LONG," +
                "CREATED VARCHAR(100)," +
                "TIME LONG," +
                "COMMENT VARCHAR(1000)," +
                "FOREIGN KEY (AUTHOR_USER_ID) REFERENCES USERS(USER_ID)," +
                "FOREIGN KEY (TASK_ID) REFERENCES TASKS(TASK_ID)" +
                ");";

        jdbcTemplate.update(sqlGroup, new MapSqlParameterSource());
    }

    @Override
    public Log addLog(Log log) {
        String sql = "insert into LOGS (AUTHOR_USER_ID, TASK_ID, CREATED, TIME, COMMENT) " +
                "VALUES ( :authorId, :taskId, :createdDate, :time, :comment );";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("authorId", log.getAuthorUserId())
                .addValue("taskId", log.getTaskId())
                .addValue("createdDate", log.getCreatedTime())
                .addValue("time", log.getTime())
                .addValue("comment", log.getComment());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, param, keyHolder);
        log.setId(keyHolder.getKey().longValue());
        return log;
    }

    @Override
    public List<Log> getLogsByTaskId(Long taskId) {
        String sql = "select * from LOGS where TASK_ID = :id ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", taskId);
        return jdbcTemplate.query(sql, param, logListExtractor);
    }

    @Override
    public Log getLogById(Long id) {
        String sql = "select * from LOGS where LOG_ID = :id ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.query(sql, param, logExtractor);
    }
}
