package v.nevolin.repository;

import v.nevolin.model.db.Task;

import java.util.List;

public interface TaskRepository {
    Task addTask(Task task);

    Task getTaskById(Long taskId);

    Task updateTask(Task task);

    List<Task> getTasksByDashboardId(Long id);
}
