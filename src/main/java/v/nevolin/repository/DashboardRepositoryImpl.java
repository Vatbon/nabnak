package v.nevolin.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import v.nevolin.model.db.Dashboard;
import v.nevolin.repository.extractor.DashboardExtractor;

@Repository("DashboardRepositoryImpl")
public class DashboardRepositoryImpl implements DashboardRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DashboardExtractor dashboardExtractor;

    @Autowired
    public DashboardRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        dashboardExtractor = new DashboardExtractor();
        init();
    }

    private void init() {
        String sqlGroup = "CREATE TABLE IF NOT EXISTS DASHBOARDS(" +
                "DASHBOARD_ID IDENTITY PRIMARY KEY," +
                "HASH VARCHAR(128) UNIQUE," +
                "NAME VARCHAR(256)" +
                ");";
        jdbcTemplate.update(sqlGroup, new MapSqlParameterSource());
    }

    @Override
    public Dashboard addDashboard(Dashboard dashboard) {
        String sql = "insert into DASHBOARDS (HASH, NAME) values (:hash, :name);";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("hash", dashboard.getHash())
                .addValue("name", dashboard.getName());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, param, keyHolder);
        dashboard.setId(keyHolder.getKey().longValue());
        return dashboard;

    }

    @Override
    public Dashboard getDashboardById(Long id) {
        String sql = "select * from DASHBOARDS where DASHBOARD_ID = :id ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.query(sql, param, dashboardExtractor);
    }

    @Override
    public Dashboard updateDashboard(Dashboard dashboard) {
        String sql = "update DASHBOARDS " +
                "set NAME = :name " +
                "where HASH = :hash ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("name", dashboard.getName())
                .addValue("hash", dashboard.getHash());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, param, keyHolder);
        dashboard.setId(keyHolder.getKey().longValue());
        return dashboard;
    }

    @Override
    public Dashboard getDashboardByHash(String hash) {
        String sql = "select * from DASHBOARDS where HASH = :hash ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("hash", hash);
        return jdbcTemplate.query(sql, param, dashboardExtractor);
    }
}
