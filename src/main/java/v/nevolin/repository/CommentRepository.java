package v.nevolin.repository;

import v.nevolin.model.db.Comment;

import java.util.List;

public interface CommentRepository {
    Comment addComment(Comment comment);

    List<Comment> getCommentsByTaskId(Long taskId);

    Comment getCommentById(Long id);

    Comment updateComment(Comment comment);
}
