package v.nevolin.repository;

import v.nevolin.model.db.DashboardRole;

import java.util.List;

public interface DashboardRoleRepository {
    List<DashboardRole> getAllUsersByDashboardId(Long id);

    DashboardRole addUserToDashboard(DashboardRole dashboardRole);

    int deleteUserFromDashboard(Long dashboardId, Long userId);

    DashboardRole changeUserRole(DashboardRole dashboardRole);

    List<DashboardRole> getAllUsersDashboards(Long userId);
}
