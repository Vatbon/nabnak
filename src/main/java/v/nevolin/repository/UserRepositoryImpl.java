package v.nevolin.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import v.nevolin.model.db.User;
import v.nevolin.repository.extractor.UserExtractor;

@Repository("UserRepositoryImpl")
public class UserRepositoryImpl implements UserRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final UserExtractor userExtractor;

    @Autowired
    public UserRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.userExtractor = new UserExtractor();
        init();
    }

    private void init() {

        String sqlGroup = "CREATE TABLE IF NOT EXISTS USERS (" +
                "USER_ID   IDENTITY PRIMARY KEY ," +
                "USERNAME  VARCHAR(64)," +
                "PASSWORD  VARCHAR(64)," +
                "EMAIL     VARCHAR(256)" +
                ");";

        jdbcTemplate.update(sqlGroup, new MapSqlParameterSource());
    }

    @Override
    public User addUser(User user) {
        String sql = "insert into USERS (USERNAME, PASSWORD, EMAIL) values (:name, :password, :email) ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("name", user.getName())
                .addValue("password", user.getPassword())
                .addValue("email", user.getEmail());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, param, keyHolder);
        user.setId(keyHolder.getKey().longValue());
        return user;
    }

    @Override
    public User getUserById(Long id) {
        String sql = "select * from USERS where USER_ID = :id ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.query(sql, param, userExtractor);
    }

    @Override
    public User getUserByName(String username) {
        String sql = "select * from USERS where USERNAME= :name ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("name", username);
        return jdbcTemplate.query(sql, param, userExtractor);
    }
}
