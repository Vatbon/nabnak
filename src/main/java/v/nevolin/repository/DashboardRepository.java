package v.nevolin.repository;

import v.nevolin.model.db.Dashboard;

public interface DashboardRepository {
    Dashboard addDashboard(Dashboard dashboard);

    Dashboard getDashboardById(Long id);

    Dashboard updateDashboard(Dashboard dashboard);

    Dashboard getDashboardByHash(String hash);
}
