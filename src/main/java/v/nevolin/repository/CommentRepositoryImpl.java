package v.nevolin.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import v.nevolin.model.db.Comment;
import v.nevolin.repository.extractor.CommentExtractor;
import v.nevolin.repository.extractor.CommentListExtractor;

import java.util.List;

@Repository("CommentRepositoryImpl")
@DependsOn({"UserRepositoryImpl", "TaskRepositoryImpl"})
public class CommentRepositoryImpl implements CommentRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final CommentExtractor commentExtractor;
    private final CommentListExtractor commentListExtractor;

    @Autowired
    public CommentRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        commentExtractor = new CommentExtractor();
        commentListExtractor = new CommentListExtractor();
        init();
    }

    private void init() {
        String sqlGroup = "CREATE TABLE IF NOT EXISTS COMMENTS(" +
                "COMMENT_ID IDENTITY PRIMARY KEY," +
                "AUTHOR_USER_ID LONG," +
                "COMMENT_TASK_ID LONG," +
                "COMMENT VARCHAR(1000)," +
                "CREATED VARCHAR(100)," +
                "FOREIGN KEY (AUTHOR_USER_ID) REFERENCES USERS(USER_ID)," +
                "FOREIGN KEY (COMMENT_TASK_ID) REFERENCES TASKS(TASK_ID)" +
                ");";

        jdbcTemplate.update(sqlGroup, new MapSqlParameterSource());
    }

    @Override
    public Comment addComment(Comment comment) {
        String sql = "insert into COMMENTS (AUTHOR_USER_ID, COMMENT_TASK_ID, COMMENT, CREATED)" +
                " VALUES ( :authorId, :taskId, :comment, :createdDate );";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("authorId", comment.getAuthorUserId())
                .addValue("taskId", comment.getTaskId())
                .addValue("comment", comment.getBody())
                .addValue("createdDate", comment.getCreatedTime());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, param, keyHolder);
        comment.setId(keyHolder.getKey().longValue());
        return comment;
    }

    @Override
    public List<Comment> getCommentsByTaskId(Long taskId) {
        String sql = "select * from COMMENTS where COMMENT_TASK_ID = :id;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", taskId);
        return jdbcTemplate.query(sql, param, commentListExtractor);
    }

    @Override
    public Comment getCommentById(Long id) {
        String sql = "select * from COMMENTS where COMMENT_ID = :id;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.query(sql, param, commentExtractor);
    }

    @Override
    public Comment updateComment(Comment comment) {
        String sql = "update COMMENTS " +
                "set COMMENT = :comment " +
                "where COMMENT_ID = :id ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("comment", comment.getBody())
                .addValue("id", comment.getId());
        jdbcTemplate.update(sql, param);
        return comment;
    }
}
