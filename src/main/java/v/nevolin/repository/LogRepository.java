package v.nevolin.repository;

import v.nevolin.model.db.Log;

import java.util.List;

public interface LogRepository {
    Log addLog(Log log);

    List<Log> getLogsByTaskId(Long taskId);

    Log getLogById(Long id);
}
