package v.nevolin.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import v.nevolin.model.db.Comment;
import v.nevolin.model.db.Log;
import v.nevolin.model.db.Task;
import v.nevolin.repository.extractor.CommentListExtractor;
import v.nevolin.repository.extractor.LogListExtractor;
import v.nevolin.repository.extractor.TaskListExtractor;

import java.util.List;

@Repository
@DependsOn({"TaskRepositoryImpl", "CommentRepositoryImpl", "LogRepositoryImpl"})
public class FullTextRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final TaskListExtractor taskListExtractor;
    private final LogListExtractor logListExtractor;
    private final CommentListExtractor commentListExtractor;
    private static boolean isInitialized = false;

    @Autowired
    public FullTextRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        taskListExtractor = new TaskListExtractor();
        logListExtractor = new LogListExtractor();
        commentListExtractor = new CommentListExtractor();
        init();
    }

    private void init() {
        if (!isInitialized) {
            String sql = "CREATE ALIAS IF NOT EXISTS FT_INIT FOR \"org.h2.fulltext.FullText.init\";" +
                    "CALL FT_INIT();" +
                    "CALL FT_CREATE_INDEX('PUBLIC', 'TASKS', 'DESCRIPTION, NAME');" +
                    "CALL FT_CREATE_INDEX('PUBLIC', 'LOGS', 'COMMENT');" +
                    "CALL FT_CREATE_INDEX('PUBLIC', 'COMMENTS', 'COMMENT');";
            jdbcTemplate.update(sql, new MapSqlParameterSource());

            isInitialized = true;
        }
    }

    public List<Task> findTasks(String text, Long userId, Long dashboardId) {
        String sql = "SELECT T.* FROM ft_search_data(:text, 0, 0) FT " +
                "JOIN TASKS T ON T.TASK_ID = FT.KEYS[1] " +
                "WHERE FT.\"TABLE\" = 'TASKS' " +
                "AND T.DASHBOARD_ID IN " +
                "(SELECT DASHBOARD_ID FROM DASHBOARD_USERS_ROLES " +
                "WHERE USER_ID = :userId AND DASHBOARD_ID = :dashboardId);";
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("text", text)
                .addValue("userId", userId)
                .addValue("dashboardId", dashboardId);
        return jdbcTemplate.query(sql, params, taskListExtractor);
    }

    public List<Log> findLogs(String text, Long userId, Long dashboardId) {
        String sql = "SELECT L.* FROM ft_search_data(:text, 0, 0) FT  " +
                "JOIN LOGS L ON L.LOG_ID = FT.KEYS[1] " +
                "JOIN TASKS T ON T.TASK_ID = L.TASK_ID " +
                "WHERE FT.\"TABLE\" = 'LOGS' " +
                "AND T.DASHBOARD_ID IN " +
                "(SELECT DASHBOARD_ID FROM DASHBOARD_USERS_ROLES " +
                "WHERE USER_ID = :userId AND DASHBOARD_ID = :dashboardId);";
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("text", text)
                .addValue("userId", userId)
                .addValue("dashboardId", dashboardId);
        return jdbcTemplate.query(sql, params, logListExtractor);
    }

    public List<Comment> findComments(String text, Long userId, Long dashboardId) {
        String sql = "SELECT C.* FROM ft_search_data(:text, 0, 0) FT " +
                "JOIN COMMENTS C ON C.COMMENT_ID  = FT.KEYS[1] " +
                "JOIN TASKS T ON T.TASK_ID = C.COMMENT_TASK_ID " +
                "WHERE FT.\"TABLE\" = 'COMMENTS' " +
                "AND T.DASHBOARD_ID IN " +
                "(SELECT DASHBOARD_ID FROM DASHBOARD_USERS_ROLES " +
                "WHERE USER_ID = :userId AND DASHBOARD_ID = :dashboardId);";
        MapSqlParameterSource params = new MapSqlParameterSource()
                .addValue("text", text)
                .addValue("userId", userId)
                .addValue("dashboardId", dashboardId);
        return jdbcTemplate.query(sql, params, commentListExtractor);
    }
}
