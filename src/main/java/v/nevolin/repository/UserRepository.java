package v.nevolin.repository;

import v.nevolin.model.db.User;

public interface UserRepository {
    User addUser(User user);

    User getUserById(Long id);

    User getUserByName(String username);
}
