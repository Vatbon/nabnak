package v.nevolin.repository;

import v.nevolin.model.db.Tag;

import java.util.List;

public interface TagRepository {
    List<Tag> getTaskTags(Long taskId);

    Tag addTag(Tag tag);

    int deleteTag(Tag tag);
}
