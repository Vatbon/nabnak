package v.nevolin.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import v.nevolin.model.db.State;
import v.nevolin.repository.extractor.StateExtractor;
import v.nevolin.repository.extractor.StateListExtractor;

import java.util.List;

@Repository("StateRepositoryImpl")
public class StateRepositoryImpl implements StateRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final StateListExtractor stateListExtractor;
    private final StateExtractor stateExtractor;
    private static boolean init = false;

    @Autowired
    public StateRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        stateListExtractor = new StateListExtractor();
        stateExtractor = new StateExtractor();
        init();
    }

    private void init() {
        String sqlGroup = "CREATE TABLE IF NOT EXISTS STATES(" +
                "STATE_ID IDENTITY PRIMARY KEY," +
                "NAME VARCHAR(256)" +
                ");";

        if (!init) {
            sqlGroup += "INSERT INTO STATES VALUES (0, 'To Do');" +
                    "INSERT INTO STATES VALUES (1, 'In Progress');" +
                    "INSERT INTO STATES VALUES (2, 'Done')";
            init = true;
        }

        jdbcTemplate.update(sqlGroup, new MapSqlParameterSource());
    }

    @Override
    public List<State> getAllStates() {
        String sql = "select * from STATES ;";
        return jdbcTemplate.query(sql, stateListExtractor);
    }

    @Override
    public State getStateById(Long id) {
        String sql = "select * from STATES where STATE_ID = :id;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.query(sql, param, stateExtractor);
    }
}
