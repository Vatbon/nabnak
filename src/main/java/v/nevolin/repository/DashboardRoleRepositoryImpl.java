package v.nevolin.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import v.nevolin.model.db.DashboardRole;
import v.nevolin.repository.extractor.DashboardRoleListExtractor;

import java.util.List;

@Repository("DashboardRoleRepositoryImpl")
@DependsOn({"DashboardRepositoryImpl", "UserRepositoryImpl", "RoleRepositoryImpl"})
public class DashboardRoleRepositoryImpl implements DashboardRoleRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DashboardRoleListExtractor dashboardRoleListExtractor;

    @Autowired
    public DashboardRoleRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        dashboardRoleListExtractor = new DashboardRoleListExtractor();
        init();
    }

    private void init() {
        String sql = "CREATE TABLE IF NOT EXISTS DASHBOARD_USERS_ROLES(" +
                "DASHBOARD_ID LONG," +
                "USER_ID LONG," +
                "ROLE_ID LONG," +
                "FOREIGN KEY (DASHBOARD_ID) REFERENCES DASHBOARDS(DASHBOARD_ID)," +
                "FOREIGN KEY (USER_ID) REFERENCES USERS(USER_ID)," +
                "FOREIGN KEY (ROLE_ID) REFERENCES ROLES(ROLE_ID));";
        jdbcTemplate.update(sql, new MapSqlParameterSource());
    }

    @Override
    public List<DashboardRole> getAllUsersByDashboardId(Long id) {
        String sql = "select * from DASHBOARD_USERS_ROLES where DASHBOARD_ID = :id ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.query(sql, param, dashboardRoleListExtractor);
    }

    @Override
    public DashboardRole addUserToDashboard(DashboardRole dashboardRole) {
        String sql = "insert into DASHBOARD_USERS_ROLES (DASHBOARD_ID, USER_ID, ROLE_ID)" +
                " values ( :dashboardId, :userId, :roleId )";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("dashboardId", dashboardRole.getDashboardId())
                .addValue("userId", dashboardRole.getUserId())
                .addValue("roleId", dashboardRole.getRoleId());
        jdbcTemplate.update(sql, param);
        return dashboardRole;
    }

    @Override
    public int deleteUserFromDashboard(Long dashboardId, Long userId) {
        String sql = "delete from DASHBOARD_USERS_ROLES" +
                " where DASHBOARD_ID = :dashboardId and USER_ID = :userId ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("dashboardId", dashboardId)
                .addValue("userId", userId);
        return jdbcTemplate.update(sql, param);
    }

    @Override
    public DashboardRole changeUserRole(DashboardRole dashboardRole) {
        String sql = "update DASHBOARD_USERS_ROLES " +
                "set ROLE_ID = :roleId " +
                "where DASHBOARD_ID = :dashboardId and USER_ID = :userId;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("roleId", dashboardRole.getRoleId())
                .addValue("dashboardId", dashboardRole.getDashboardId())
                .addValue("userId", dashboardRole.getUserId());
        jdbcTemplate.update(sql, param);
        return dashboardRole;
    }

    @Override
    public List<DashboardRole> getAllUsersDashboards(Long userId) {
        String sql = "select * from DASHBOARD_USERS_ROLES where USER_ID = :id ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", userId);
        return jdbcTemplate.query(sql, param, dashboardRoleListExtractor);
    }
}
