package v.nevolin.repository.extractor;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import v.nevolin.model.db.Task;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TaskListExtractor implements ResultSetExtractor<List<Task>> {
    @Override
    public List<Task> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<Task> taskList = new ArrayList<>();
        while (rs.next()) {
            Task task = new Task(rs.getLong("TASK_ID"),
                    rs.getLong("CREATED_BY_USER_ID"),
                    rs.getLong("ASSIGNED_USER_ID") == 0 ?
                            null : rs.getLong("ASSIGNED_USER_ID"),
                    rs.getLong("DASHBOARD_ID"),
                    rs.getString("NAME"),
                    rs.getString("DESCRIPTION"),
                    rs.getString("CREATED"),
                    rs.getString("LAST_UPDATED"),
                    rs.getLong("PARENT_TASK_ID") == 0 ?
                            null : rs.getLong("PARENT_TASK_ID"),
                    rs.getLong("STATE_ID"));
            taskList.add(task);
        }
        return taskList;
    }
}
