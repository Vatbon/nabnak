package v.nevolin.repository.extractor;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import v.nevolin.model.db.Dashboard;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DashboardExtractor implements ResultSetExtractor<Dashboard> {
    @Override
    public Dashboard extractData(ResultSet rs) throws SQLException, DataAccessException {
        Dashboard dashboard = null;
        if (rs.next()) {
            dashboard = new Dashboard(rs.getLong("DASHBOARD_ID"),
                    rs.getString("HASH"),
                    rs.getString("NAME"));
        }
        return dashboard;
    }
}
