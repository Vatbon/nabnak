package v.nevolin.repository.extractor;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import v.nevolin.model.db.Log;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LogListExtractor implements ResultSetExtractor<List<Log>> {
    @Override
    public List<Log> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<Log> logList = new ArrayList<>();
        while (rs.next()) {
            Log log = new Log(rs.getLong("LOG_ID"),
                    rs.getLong("AUTHOR_USER_ID"),
                    rs.getLong("TASK_ID"),
                    rs.getString("CREATED"),
                    rs.getInt("TIME"),
                    rs.getString("COMMENT"));
            logList.add(log);
        }
        return logList;
    }
}
