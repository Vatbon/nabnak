package v.nevolin.repository.extractor;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import v.nevolin.model.db.Role;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RoleListExtractor implements ResultSetExtractor<List<Role>> {
    @Override
    public List<Role> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<Role> roleList = new ArrayList<>();
        while (rs.next()) {
            Role role = new Role(rs.getLong("ROLE_ID"),
                    rs.getString("NAME"));
            roleList.add(role);
        }
        return roleList;
    }
}
