package v.nevolin.repository.extractor;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import v.nevolin.model.db.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserExtractor implements ResultSetExtractor<User> {
    @Override
    public User extractData(ResultSet rs) throws SQLException, DataAccessException {
        User user = null;
        if (rs.next()) {
            user = new User(rs.getLong("USER_ID"),
                    rs.getString("USERNAME"),
                    rs.getString("EMAIL"),
                    rs.getString("PASSWORD"));
        }
        return user;
    }
}
