package v.nevolin.repository.extractor;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import v.nevolin.model.db.Role;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleExtractor implements ResultSetExtractor<Role> {
    @Override
    public Role extractData(ResultSet rs) throws SQLException, DataAccessException {
        Role role = null;
        if (rs.next()) {
            role = new Role(rs.getLong("ROLE_ID"),
                    rs.getString("NAME"));
        }
        return role;
    }
}
