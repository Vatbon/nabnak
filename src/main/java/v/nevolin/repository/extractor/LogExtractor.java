package v.nevolin.repository.extractor;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import v.nevolin.model.db.Log;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LogExtractor implements ResultSetExtractor<Log> {
    @Override
    public Log extractData(ResultSet rs) throws SQLException, DataAccessException {
        Log log = null;
        if (rs.next()) {
            log = new Log(rs.getLong("LOG_ID"),
                    rs.getLong("AUTHOR_USER_ID"),
                    rs.getLong("TASK_ID"),
                    rs.getString("CREATED"),
                    rs.getInt("TIME"),
                    rs.getString("COMMENT"));
        }
        return log;
    }
}
