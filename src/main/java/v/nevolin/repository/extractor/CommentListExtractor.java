package v.nevolin.repository.extractor;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import v.nevolin.model.db.Comment;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentListExtractor implements ResultSetExtractor<List<Comment>> {
    @Override
    public List<Comment> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<Comment> commentList = new ArrayList<>();
        while (rs.next()) {
            Comment comment = new Comment(rs.getLong("COMMENT_ID"),
                    rs.getLong("AUTHOR_USER_ID"),
                    rs.getLong("COMMENT_TASK_ID"),
                    rs.getString("COMMENT"),
                    rs.getString("CREATED"));
            commentList.add(comment);
        }
        return commentList;
    }
}
