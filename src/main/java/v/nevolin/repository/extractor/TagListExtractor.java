package v.nevolin.repository.extractor;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import v.nevolin.model.db.Tag;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TagListExtractor implements ResultSetExtractor<List<Tag>> {
    @Override
    public List<Tag> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<Tag> tagList = new ArrayList<>();
        while (rs.next()) {
            Tag tag = new Tag(rs.getLong("TASK_ID"),
                    rs.getString("NAME"));
            tagList.add(tag);
        }
        return tagList;
    }
}
