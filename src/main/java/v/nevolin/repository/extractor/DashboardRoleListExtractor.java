package v.nevolin.repository.extractor;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import v.nevolin.model.db.DashboardRole;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DashboardRoleListExtractor implements ResultSetExtractor<List<DashboardRole>> {
    @Override
    public List<DashboardRole> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<DashboardRole> dashboardRoleList = new ArrayList<>();
        while (rs.next()) {
            DashboardRole dashboardRole = new DashboardRole(rs.getLong("DASHBOARD_ID"),
                    rs.getLong("USER_ID"),
                    rs.getLong("ROLE_ID"));
            dashboardRoleList.add(dashboardRole);
        }
        return dashboardRoleList;
    }
}
