package v.nevolin.repository.extractor;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import v.nevolin.model.db.State;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StateListExtractor implements ResultSetExtractor<List<State>> {
    @Override
    public List<State> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<State> stateList = new ArrayList<>();
        while (rs.next()) {
            State state = new State(rs.getLong("STATE_ID"),
                    rs.getString("NAME"));
            stateList.add(state);
        }
        return stateList;
    }
}
