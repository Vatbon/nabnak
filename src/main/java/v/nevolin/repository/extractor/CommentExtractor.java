package v.nevolin.repository.extractor;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import v.nevolin.model.db.Comment;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CommentExtractor implements ResultSetExtractor<Comment> {
    @Override
    public Comment extractData(ResultSet rs) throws SQLException, DataAccessException {
        Comment comment = null;
        if (rs.next()) {
            comment = new Comment(rs.getLong("COMMENT_ID"),
                    rs.getLong("AUTHOR_USER_ID"),
                    rs.getLong("COMMENT_TASK_ID"),
                    rs.getString("COMMENT"),
                    rs.getString("CREATED"));
        }
        return comment;
    }
}
