package v.nevolin.repository.extractor;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import v.nevolin.model.db.State;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StateExtractor implements ResultSetExtractor<State> {
    @Override
    public State extractData(ResultSet rs) throws SQLException, DataAccessException {
        State state = null;
        if (rs.next()) {
            state = new State(rs.getLong("STATE_ID"),
                    rs.getString("NAME"));
        }
        return state;
    }
}
