package v.nevolin.repository;

import v.nevolin.model.db.State;

import java.util.List;

public interface StateRepository {
    List<State> getAllStates();

    State getStateById(Long id);
}
