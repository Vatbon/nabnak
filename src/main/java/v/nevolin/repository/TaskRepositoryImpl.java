package v.nevolin.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import v.nevolin.model.db.Task;
import v.nevolin.repository.extractor.TaskExtractor;
import v.nevolin.repository.extractor.TaskListExtractor;

import java.util.List;

@Repository("TaskRepositoryImpl")
@DependsOn({"UserRepositoryImpl", "DashboardRepositoryImpl", "StateRepositoryImpl"})
public class TaskRepositoryImpl implements TaskRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final TaskExtractor taskExtractor;
    private final TaskListExtractor taskListExtractor;
    private static boolean init = false;

    @Autowired
    public TaskRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        taskExtractor = new TaskExtractor();
        taskListExtractor = new TaskListExtractor();
        init();
    }

    private void init() {
        String sql = "CREATE TABLE IF NOT EXISTS TASKS (" +
                "TASK_ID IDENTITY PRIMARY KEY," +
                "CREATED_BY_USER_ID LONG," +
                "ASSIGNED_USER_ID LONG," +
                "DASHBOARD_ID LONG," +
                "NAME VARCHAR(256)," +
                "DESCRIPTION VARCHAR(1000)," +
                "CREATED VARCHAR(100)," +
                "LAST_UPDATED VARCHAR(100)," +
                "PARENT_TASK_ID LONG," +
                "STATE_ID LONG," +
                "FOREIGN KEY (CREATED_BY_USER_ID) REFERENCES USERS(USER_ID)," +
                "FOREIGN KEY (ASSIGNED_USER_ID) REFERENCES USERS(USER_ID)," +
                "FOREIGN KEY (DASHBOARD_ID) REFERENCES DASHBOARDS(DASHBOARD_ID)," +
                "FOREIGN KEY (STATE_ID) REFERENCES STATES(STATE_ID)" +
                ");";
        if (!init) {
            sql += "ALTER TABLE TASKS ADD CONSTRAINT PARENT_TASK_ID_CONSTRAINT " +
                    "FOREIGN KEY (PARENT_TASK_ID) REFERENCES TASKS(TASK_ID);";
            init = true;
        }
        jdbcTemplate.update(sql, new MapSqlParameterSource());
    }

    @Override
    public Task addTask(Task task) {
        String sql = "insert into TASKS (CREATED_BY_USER_ID, ASSIGNED_USER_ID, DASHBOARD_ID," +
                " NAME, DESCRIPTION, CREATED, LAST_UPDATED, PARENT_TASK_ID, STATE_ID)" +
                " values (:createdById, :assignedToId, :dashboardId," +
                " :name, :description, :createdDate, :lastUpdate, :parentId, :stateId);";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("createdById", task.getCreatedByUserId())
                .addValue("assignedToId", task.getAssignedToUserId())
                .addValue("dashboardId", task.getDashboardId())
                .addValue("name", task.getName())
                .addValue("description", task.getDescription())
                .addValue("createdDate", task.getCreatedTime())
                .addValue("lastUpdate", task.getLastUpdatedTime())
                .addValue("parentId", task.getParentTaskId())
                .addValue("stateId", task.getStateId());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, param, keyHolder);
        return getTaskById(keyHolder.getKey().longValue());
    }

    @Override
    public Task getTaskById(Long taskId) {
        String sql = "select * from TASKS where TASK_ID = :id ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", taskId);
        return jdbcTemplate.query(sql, param, taskExtractor);
    }

    @Override
    public Task updateTask(Task task) {
        String sql = "update TASKS " +
                "set NAME = :name, DESCRIPTION = :description, ASSIGNED_USER_ID = :assignedId, " +
                "LAST_UPDATED = :lastUpdate, STATE_ID = :stateId " +
                "where TASK_ID = :id ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("name", task.getName())
                .addValue("description", task.getDescription())
                .addValue("lastUpdate", task.getLastUpdatedTime())
                .addValue("stateId", task.getStateId())
                .addValue("assignedId", task.getAssignedToUserId())
                .addValue("id", task.getId());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, param, keyHolder);
        return getTaskById(task.getId());
    }

    @Override
    public List<Task> getTasksByDashboardId(Long dashboardId) {
        String sql = "select * from TASKS where DASHBOARD_ID = :id ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", dashboardId);
        return jdbcTemplate.query(sql, param, taskListExtractor);
    }
}
