package v.nevolin.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import v.nevolin.model.db.Tag;
import v.nevolin.repository.extractor.TagListExtractor;

import java.util.List;

@Repository("TagRepositoryImpl")
@DependsOn("TaskRepositoryImpl")
public class TagRepositoryImpl implements TagRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final TagListExtractor tagListExtractor;

    @Autowired
    public TagRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        tagListExtractor = new TagListExtractor();
        init();
    }

    private void init() {
        String sqlGroup = "CREATE TABLE IF NOT EXISTS TAGS(" +
                "TASK_ID LONG ," +
                "NAME VARCHAR(256)," +
                "FOREIGN KEY (TASK_ID) REFERENCES TASKS(TASK_ID)," +
                "UNIQUE (TASK_ID, NAME));";

        jdbcTemplate.update(sqlGroup, new MapSqlParameterSource());
    }

    @Override
    public List<Tag> getTaskTags(Long taskId) {
        String sql = "select * from TAGS where TASK_ID = :id;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", taskId);
        return jdbcTemplate.query(sql, param, tagListExtractor);
    }

    @Override
    public Tag addTag(Tag tag) {
        deleteTag(tag);
        String sql = "insert into TAGS (TASK_ID, NAME) values (:taskId, :name);";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("taskId", tag.getTaskId())
                .addValue("name", tag.getName());
        jdbcTemplate.update(sql, param);
        return tag;
    }

    @Override
    public int deleteTag(Tag tag) {
        String sql = "delete from TAGS " +
                "where TASK_ID = :taskId and NAME = :name;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("taskId", tag.getTaskId())
                .addValue("name", tag.getName());
        return jdbcTemplate.update(sql, param);
    }
}
