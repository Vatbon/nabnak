package v.nevolin.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import v.nevolin.model.db.Role;
import v.nevolin.repository.extractor.RoleExtractor;
import v.nevolin.repository.extractor.RoleListExtractor;

import java.util.List;

@Repository("RoleRepositoryImpl")
public class RoleRepositoryImpl implements RoleRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final RoleListExtractor roleListExtractor;
    private final RoleExtractor roleExtractor;
    private static boolean init = false;

    @Autowired
    public RoleRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        roleListExtractor = new RoleListExtractor();
        roleExtractor = new RoleExtractor();
        init();
    }

    private void init() {
        String sqlGroup = "CREATE TABLE IF NOT EXISTS ROLES(" +
                "ROLE_ID IDENTITY PRIMARY KEY," +
                "NAME VARCHAR(256)" +
                ");";

        if (!init) {
            sqlGroup += "INSERT INTO ROLES VALUES (0, 'Admin');" +
                    "INSERT INTO ROLES VALUES (1, 'Participant');" +
                    "INSERT INTO ROLES VALUES (2, 'Guest');";
            init = true;
        }

        jdbcTemplate.update(sqlGroup, new MapSqlParameterSource());
    }

    @Override
    public List<Role> getAllRoles() {
        String sql = "select * from ROLES;";
        return jdbcTemplate.query(sql, roleListExtractor);
    }

    @Override
    public Role getRoleById(Long id) {
        String sql = "select * from ROLES where ROLE_ID = :id ;";
        MapSqlParameterSource param = new MapSqlParameterSource()
                .addValue("id", id);
        return jdbcTemplate.query(sql, param, roleExtractor);
    }
}
