package v.nevolin.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HashGenerator {
    private static final List<Character> allowedCharacter = makeAllowed();

    private static List<Character> makeAllowed() {
        String allowed = "0123456789qwertyuiopasdfghjklzxcvbnm";
        List<Character> result = new ArrayList<>();
        for (char c : allowed.toCharArray()) {
            result.add(c);
        }
        return result;
    }

    private static final List<String> strings = new ArrayList<>();
    private static final int MAX_LIST_SIZE = 10000;
    private static final int MAX_LENGTH = 64;

    public static String randomString(int length) {
        if (strings.size() > MAX_LIST_SIZE) {
            strings.clear();
        }
        length = Math.min(length, MAX_LENGTH);
        Random random = new Random();
        String result = "";
        do {
            for (int i = 0; i < length; i++) {
                result = result.concat(String.valueOf(allowedCharacter.get(random.nextInt(allowedCharacter.size()))));
            }
        } while (strings.contains(result));
        strings.add(result);
        return result;
    }
}
