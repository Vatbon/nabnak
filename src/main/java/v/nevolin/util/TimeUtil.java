package v.nevolin.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {

    private static final String PATTERN = "d MMM yyyy, HH:mm:ss Z";

    private TimeUtil() {
    }

    private static SimpleDateFormat getSimpleDateFormat() {
        return new SimpleDateFormat(PATTERN);
    }

    public static String getCurrentTime() {
        return getSimpleDateFormat().format(new Date());
    }
}
