package v.nevolin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import v.nevolin.exception.ServiceException;
import v.nevolin.model.api.TaskDto;
import v.nevolin.model.db.DashboardRole;
import v.nevolin.model.db.Tag;
import v.nevolin.model.db.Task;
import v.nevolin.model.db.User;
import v.nevolin.repository.StateRepository;
import v.nevolin.repository.TagRepository;
import v.nevolin.repository.TaskRepository;
import v.nevolin.util.TimeUtil;

import java.util.List;

@Service
public class TaskService {
    private final TaskRepository taskRepository;
    private final TagRepository tagRepository;
    private final StateRepository stateRepository;
    private final TagService tagService;
    private final UserService userService;
    private final DashboardService dashboardService;

    @Autowired
    public TaskService(TaskRepository taskRepository,
                       TagRepository tagRepository,
                       StateRepository stateRepository,
                       TagService tagService,
                       UserService userService,
                       DashboardService dashboardService) {
        this.taskRepository = taskRepository;
        this.tagRepository = tagRepository;
        this.stateRepository = stateRepository;
        this.tagService = tagService;
        this.userService = userService;
        this.dashboardService = dashboardService;
    }

    public TaskDto createTask(Task task) {
        validateTask(task);
        task.setCreatedTime(TimeUtil.getCurrentTime());
        task.setLastUpdatedTime(TimeUtil.getCurrentTime());
        Task newTask = taskRepository.addTask(task);
        TaskDto taskDto = new TaskDto();
        taskDto.setTask(newTask);
        taskDto.setTagList(tagRepository.getTaskTags(newTask.getId()));
        return taskDto;
    }

    public TaskDto changeStateOfTask(Long taskId, Long stateId) {
        Task task = taskRepository.getTaskById(taskId);
        if (task == null) {
            throw new ServiceException("No such task");
        }

        if (stateRepository.getStateById(stateId) != null) {
            task.setStateId(stateId);
            task.setLastUpdatedTime(TimeUtil.getCurrentTime());
            task = taskRepository.updateTask(task);
        }
        TaskDto taskDto = new TaskDto();
        taskDto.setTask(getTaskById(taskId));
        taskDto.setTagList(tagRepository.getTaskTags(task.getId()));
        return taskDto;
    }

    public TaskDto assignTask(Long taskId, Long userId) {
        Task task = taskRepository.getTaskById(taskId);
        /*no such task*/
        if (task == null) {
            throw new ServiceException("No such task");
        }
        User user = userService.getUserById(userId);
        /*no such user*/
        if (user == null) {
            throw new ServiceException("No such user");
        }
        List<DashboardRole> dashboards = dashboardService.getUsersDashboards(userId);
        boolean validDashboard = false;
        for (DashboardRole dashboard : dashboards) {
            if (dashboard.getUserId().equals(user.getId()) && dashboard.getDashboardId().equals(task.getDashboardId())) {
                validDashboard = true;
                break;
            }
        }
        if (!validDashboard) {
            throw new ServiceException("Not valid dashboard");
        }

        task.setLastUpdatedTime(TimeUtil.getCurrentTime());
        task.setAssignedToUserId(userId);
        task = taskRepository.updateTask(task);

        TaskDto taskDto = new TaskDto();
        taskDto.setTask(task);
        taskDto.setTagList(tagRepository.getTaskTags(task.getId()));
        return taskDto;
    }

    public TaskDto updateTask(TaskDto taskDto) {
        Task task = taskDto.getTask();
        validateTask(task);
        task.setLastUpdatedTime(TimeUtil.getCurrentTime());
        Task updateTask = taskRepository.updateTask(task);

        tagRepository.getTaskTags(updateTask.getId()).forEach(tagService::deleteTag);
        for (Tag tag : taskDto.getTagList()) {
            tagService.addTag(new Tag(updateTask.getId(), tag.getName()));
        }

        TaskDto taskDtoOut = new TaskDto();
        taskDtoOut.setTask(updateTask);
        taskDtoOut.setTagList(taskDto.getTagList());
        return taskDtoOut;
    }

    public Task getTaskById(Long taskId) {
        return taskRepository.getTaskById(taskId);
    }

    private boolean validateTask(Task task) {
        User user = userService.getUserById(task.getCreatedByUserId());
        /*no such user*/
        if (user == null) {
            throw new ServiceException("No such user");
        }

        /*if has assigned user, check its presence*/
        if (task.getAssignedToUserId() != null) {
            User assignedUser = userService.getUserById(task.getAssignedToUserId());
            if (assignedUser == null) {
                throw new ServiceException("No such user");
            }
        }

        List<DashboardRole> dashboards = dashboardService.getUsersDashboards(user.getId());
        boolean validDashboard = false;
        for (DashboardRole dashboard : dashboards) {
            if (dashboard.getDashboardId().equals(task.getDashboardId())) {
                validDashboard = true;
                break;
            }
        }
        if (!validDashboard) {
            throw new ServiceException("Not valid dashboard");
        }
        /*no such parent task*/
        if (task.getParentTaskId() != null) {
            Task parentTask = getTaskById(task.getParentTaskId());
            if (parentTask == null) {
                throw new ServiceException("No such task");
            }
        }
        if (task.getName().isEmpty()) {
            throw new ServiceException("Task cannot have blank name");
        }
        if (stateRepository.getStateById(task.getStateId()) == null) {
            throw new ServiceException("No such state");
        }
        if (task.getName().length() > 256) {
            throw new ServiceException("Task name is too long");
        }
        if (task.getDescription().length() > 1000) {
            throw new ServiceException("Task description is too long");
        }
        return true;
    }
}
