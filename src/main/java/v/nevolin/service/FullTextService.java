package v.nevolin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import v.nevolin.exception.ServiceException;
import v.nevolin.model.db.*;
import v.nevolin.repository.FullTextRepository;

import java.util.List;

@Service
public class FullTextService {
    private final UserService userService;
    private final DashboardService dashboardService;
    private final FullTextRepository fullTextRepository;

    @Autowired
    public FullTextService(UserService userService,
                           DashboardService dashboardService,
                           FullTextRepository fullTextRepository) {
        this.userService = userService;
        this.dashboardService = dashboardService;
        this.fullTextRepository = fullTextRepository;
    }

    public List<Comment> findComments(String text, Long userId, Long dashboardId) {
        validate(userId, dashboardId);
        return fullTextRepository.findComments(text, userId, dashboardId);
    }

    public List<Log> findLogs(String text, Long userId, Long dashboardId) {
        validate(userId, dashboardId);
        return fullTextRepository.findLogs(text, userId, dashboardId);
    }

    public List<Task> findTasks(String text, Long userId, Long dashboardId) {
        validate(userId, dashboardId);
        return fullTextRepository.findTasks(text, userId, dashboardId);
    }

    private void validate(Long userId, Long dashboardId) {
        User user = userService.getUserById(userId);
        if (user == null) {
            throw new ServiceException("No such user");
        }
        boolean valid = false;
        for (DashboardRole usersDashboard : dashboardService.getUsersDashboards(user.getId())) {
            if (usersDashboard.getDashboardId().equals(dashboardId)) {
                valid = true;
                break;
            }
        }
        if (!valid) {
            throw new ServiceException("No such dashboard connected with given user");
        }
    }
}
