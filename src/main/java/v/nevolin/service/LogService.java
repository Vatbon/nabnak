package v.nevolin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import v.nevolin.exception.ServiceException;
import v.nevolin.model.db.DashboardRole;
import v.nevolin.model.db.Log;
import v.nevolin.model.db.Task;
import v.nevolin.model.db.User;
import v.nevolin.repository.LogRepository;
import v.nevolin.util.TimeUtil;

import java.util.List;

@Service
public class LogService {

    private final LogRepository logRepository;
    private final TaskService taskService;
    private final UserService userService;
    private final DashboardService dashboardService;

    @Autowired
    public LogService(LogRepository logRepository,
                      TaskService taskService,
                      UserService userService,
                      DashboardService dashboardService) {
        this.logRepository = logRepository;
        this.taskService = taskService;
        this.userService = userService;
        this.dashboardService = dashboardService;
    }

    public Log createLog(Log log) {
        if (validLog(log)) {
            log.setCreatedTime(TimeUtil.getCurrentTime());
            return logRepository.addLog(log);
        }
        return null;
    }

    private boolean validLog(Log log) {
        User user = userService.getUserById(log.getAuthorUserId());
        /*if no such user*/
        if (user == null) {
            throw new ServiceException("No such user");
        }
        Task task = taskService.getTaskById(log.getTaskId());
        /*if no such task*/
        if (task == null) {
            throw new ServiceException("No such task");
        }

        if (log.getComment().length() > 1000) {
            throw new ServiceException("Log comment is too long");
        }

        boolean validParticipant = false;
        List<DashboardRole> dashboards = dashboardService.getUsersDashboards(user.getId());
        Long dashboardId = task.getDashboardId();
        for (DashboardRole dashboard : dashboards) {
            if (dashboard.getDashboardId().equals(dashboardId)) {
                validParticipant = true;
                break;
            }
        }
        boolean validData = log.getTime() > 0;

        return validData && validParticipant;
    }

    public List<Log> getLogsByTaskId(Long taskId) {
        return logRepository.getLogsByTaskId(taskId);
    }
}
