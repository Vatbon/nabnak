package v.nevolin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import v.nevolin.exception.ServiceException;
import v.nevolin.model.api.DashboardDto;
import v.nevolin.model.api.TaskDto;
import v.nevolin.model.db.*;
import v.nevolin.repository.*;
import v.nevolin.util.HashGenerator;

import java.util.ArrayList;
import java.util.List;

@Service
public class DashboardService {
    private final DashboardRepository dashboardRepository;
    private final DashboardRoleRepository dashboardRoleRepository;
    private final TaskRepository taskRepository;
    private final TagRepository tagRepository;
    private final CommentRepository commentRepository;
    private final LogRepository logRepository;
    private final UserService userService;

    @Autowired
    public DashboardService(DashboardRepository dashboardRepository,
                            DashboardRoleRepository dashboardRoleRepository,
                            TaskRepository taskRepository,
                            TagRepository tagRepository,
                            CommentRepository commentRepository,
                            LogRepository logRepository,
                            UserService userService) {
        this.dashboardRepository = dashboardRepository;
        this.dashboardRoleRepository = dashboardRoleRepository;
        this.taskRepository = taskRepository;
        this.tagRepository = tagRepository;
        this.commentRepository = commentRepository;
        this.logRepository = logRepository;
        this.userService = userService;
    }

    public List<DashboardRole> getUsersDashboards(Long userId) {
        return dashboardRoleRepository.getAllUsersDashboards(userId);
    }

    public DashboardDto buildDashboardByHash(String hash) {
        DashboardDto dashboardDto = new DashboardDto();
        Dashboard dashboard = dashboardRepository.getDashboardByHash(hash);
        if (dashboard == null) {
            throw new ServiceException("Cannot find dashboard with hash: " + hash);
        }
        List<DashboardRole> dashboardRoleList = dashboardRoleRepository.getAllUsersByDashboardId(dashboard.getId());
        List<TaskDto> taskList = prepareTaskDto(dashboard.getId());
        List<Comment> commentList = prepareComments(taskList);
        List<Log> logList = prepareLogs(taskList);
        dashboardDto.setDashboard(dashboard);
        dashboardDto.setDashboardRoleList(dashboardRoleList);
        dashboardDto.setTaskList(taskList);
        dashboardDto.setCommentList(commentList);
        dashboardDto.setLogList(logList);
        return dashboardDto;
    }

    private List<Log> prepareLogs(List<TaskDto> taskList) {
        List<Log> logList = new ArrayList<>();
        for (TaskDto taskDto : taskList) {
            logList.addAll(logRepository.getLogsByTaskId(taskDto.getTask().getId()));
        }
        return logList;
    }

    private List<Comment> prepareComments(List<TaskDto> taskList) {
        List<Comment> commentList = new ArrayList<>();
        for (TaskDto taskDto : taskList) {
            commentList.addAll(commentRepository.getCommentsByTaskId(taskDto.getTask().getId()));
        }
        return commentList;
    }

    private List<TaskDto> prepareTaskDto(Long dashboardId) {
        List<Task> tasks = taskRepository.getTasksByDashboardId(dashboardId);
        List<TaskDto> taskDtoList = new ArrayList<>();
        for (Task task : tasks) {
            TaskDto taskDto = new TaskDto();
            taskDto.setTask(task);
            taskDto.setTagList(tagRepository.getTaskTags(task.getId()));
            taskDtoList.add(taskDto);
        }
        return taskDtoList;
    }

    public void joinDashboard(String hash, Long userId) {
        Dashboard dashboard = dashboardRepository.getDashboardByHash(hash);
        if (dashboard == null) {
            throw new ServiceException("Cannot find dashboard with hash: " + hash);
        }
        for (DashboardRole role : dashboardRoleRepository.getAllUsersByDashboardId(dashboard.getId())) {
            if (role.getUserId().equals(userId)) {
                throw new ServiceException("Already joined the dashboard");
            }
        }
        DashboardRole dashboardRole = new DashboardRole();
        dashboardRole.setUserId(userId);
        dashboardRole.setRoleId(2L);
        dashboardRole.setDashboardId(dashboard.getId());
        dashboardRoleRepository.addUserToDashboard(dashboardRole);
    }

    public void leaveDashboard(String hash, Long userId) {
        Dashboard dashboard = dashboardRepository.getDashboardByHash(hash);
        dashboardRoleRepository.deleteUserFromDashboard(dashboard.getId(), userId);
    }

    public DashboardRole changeRole(String hash, Long adminId, Long userId, Long roleId) {
        Dashboard dashboard = dashboardRepository.getDashboardByHash(hash);
        DashboardRole dashboardRoleAdmin = null;
        for (DashboardRole role : dashboardRoleRepository.getAllUsersByDashboardId(dashboard.getId())) {
            if (role.getUserId().equals(adminId)) {
                dashboardRoleAdmin = role;
                break;
            }
        }
        if (dashboardRoleAdmin == null) {
            throw new ServiceException("You don't have enough rights to change roles of other users");
        }
        if (!dashboardRoleAdmin.getRoleId().equals(0L)) {
            throw new ServiceException("You don't have enough rights to change roles of other users");
        }
        DashboardRole dashboardRoleUser = new DashboardRole();
        dashboardRoleUser.setDashboardId(dashboard.getId());
        dashboardRoleUser.setRoleId(roleId);
        dashboardRoleUser.setUserId(userId);
        return dashboardRoleRepository.changeUserRole(dashboardRoleUser);
    }

    public Dashboard createDashboard(Dashboard dashboard, Long userId) {
        if (dashboard.getName().isEmpty()) {
            throw new ServiceException("Dashboard cannot have blank name");
        }
        if (userService.getUserById(userId) == null) {
            throw new ServiceException("No such user");
        }

        if (dashboard.getName().length() > 256) {
            throw new ServiceException("Dashboard name is too long");
        }
        /*generate unique hash*/
        do {
            dashboard.setHash(HashGenerator.randomString(32));
        } while (dashboardRepository.getDashboardByHash(dashboard.getHash()) != null);

        Dashboard newDashboard = dashboardRepository.addDashboard(dashboard);
        DashboardRole dashboardRole = new DashboardRole();
        dashboardRole.setUserId(userId);
        dashboardRole.setRoleId(0L);
        dashboardRole.setDashboardId(newDashboard.getId());
        dashboardRoleRepository.addUserToDashboard(dashboardRole);
        return newDashboard;
    }
}
