package v.nevolin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import v.nevolin.exception.ServiceException;
import v.nevolin.model.db.Tag;
import v.nevolin.repository.TagRepository;
import v.nevolin.repository.TaskRepository;

@Service
public class TagService {
    private final TagRepository tagRepository;
    private final TaskRepository taskRepository;

    @Autowired
    public TagService(TagRepository tagRepository, TaskRepository taskRepository) {
        this.tagRepository = tagRepository;
        this.taskRepository = taskRepository;
    }

    public void addTag(Tag tag) {
        if (validTag(tag)) {
            tagRepository.addTag(tag);
        }
    }

    private boolean validTag(Tag tag) {
        if (tag.getName().length() > 256) {
            throw new ServiceException("Tag name is too long");
        }
        return !tag.getName().isBlank() && taskRepository.getTaskById(tag.getTaskId()) != null;
    }

    public void deleteTag(Tag tag) {
        tagRepository.deleteTag(tag);
    }
}
