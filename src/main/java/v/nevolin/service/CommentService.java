package v.nevolin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import v.nevolin.exception.ServiceException;
import v.nevolin.model.db.Comment;
import v.nevolin.model.db.DashboardRole;
import v.nevolin.model.db.Task;
import v.nevolin.model.db.User;
import v.nevolin.repository.CommentRepository;
import v.nevolin.util.TimeUtil;

import java.util.List;

@Service
public class CommentService {

    private final CommentRepository commentRepository;
    private final UserService userService;
    private final DashboardService dashboardService;
    private final TaskService taskService;

    @Autowired
    public CommentService(CommentRepository commentRepository,
                          UserService userService,
                          DashboardService dashboardService,
                          TaskService taskService) {
        this.commentRepository = commentRepository;
        this.userService = userService;
        this.dashboardService = dashboardService;
        this.taskService = taskService;
    }

    public Comment createComment(Comment comment) {
        if (validComment(comment)) {
            comment.setCreatedTime(TimeUtil.getCurrentTime());
            return commentRepository.addComment(comment);
        }
        return null;
    }

    public Comment updateComment(Comment comment) {
        if (commentRepository.getCommentById(comment.getId()) == null) {
            throw new ServiceException("Empty comment");
        }
        if (validComment(comment)) {
            comment.setCreatedTime(TimeUtil.getCurrentTime());
            return commentRepository.updateComment(comment);
        }
        return null;
    }

    private boolean validComment(Comment comment) {
        User user = userService.getUserById(comment.getAuthorUserId());
        /*if no such user*/
        if (user == null) {
            throw new ServiceException("No such user");
        }
        Task task = taskService.getTaskById(comment.getTaskId());
        /*if no such task*/
        if (task == null) {
            throw new ServiceException("No such task");
        }

        boolean validParticipant = false;
        List<DashboardRole> dashboards = dashboardService.getUsersDashboards(user.getId());
        Long dashboardId = task.getDashboardId();
        for (DashboardRole dashboard : dashboards) {
            if (dashboard.getDashboardId().equals(dashboardId)) {
                validParticipant = true;
                break;
            }
        }

        if (comment.getBody().length() > 1000) {
            throw new ServiceException("Comment text is to long");
        }

        return validParticipant;
    }

    public Comment getCommentById(Long commentId) {
        return commentRepository.getCommentById(commentId);
    }

    public List<Comment> getCommentsByTaskId(Long taskId) {
        return commentRepository.getCommentsByTaskId(taskId);
    }
}
