package v.nevolin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import v.nevolin.exception.ServiceException;
import v.nevolin.model.api.UserDto;
import v.nevolin.model.db.DashboardRole;
import v.nevolin.model.db.User;
import v.nevolin.repository.DashboardRepository;
import v.nevolin.repository.DashboardRoleRepository;
import v.nevolin.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final DashboardRepository dashboardRepository;
    private final DashboardRoleRepository dashboardRoleRepository;

    @Autowired
    public UserService(UserRepository userRepository,
                       DashboardRepository dashboardRepository,
                       DashboardRoleRepository dashboardRoleRepository) {
        this.userRepository = userRepository;
        this.dashboardRepository = dashboardRepository;
        this.dashboardRoleRepository = dashboardRoleRepository;
    }

    public User registerUser(User user) {
        User maybeUser = userRepository.getUserByName(user.getName());
        if (maybeUser != null) {
            throw new ServiceException("This login is taken");
        } else if (user.getName().isEmpty()) {
            throw new ServiceException("Login cannot be blank");
        } else if (user.getPassword().isEmpty()) {
            throw new ServiceException("Password cannot be blank");
        } else if (user.getName().length() > 64) {
            throw new ServiceException("Username is too long");
        } else if (user.getPassword().length() > 64) {
            throw new ServiceException("Password is too long");
        } else if (user.getEmail().length() > 256) {
            throw new ServiceException("Email is too long");
        }
        return userRepository.addUser(user);
    }

    public User logIn(User user) {
        User maybeUser = userRepository.getUserByName(user.getName());
        if (maybeUser == null || !maybeUser.getPassword().equals(user.getPassword())) {
            throw new ServiceException("Wrong login or password");
        }
        return maybeUser;
    }

    public User getUserById(Long userId) {
        User user = userRepository.getUserById(userId);
        if (user == null) {
            throw new ServiceException("No such user");
        }
        user.setPassword("");
        return user;
    }

    public List<UserDto> getUsersByHash(String hash) {
        List<DashboardRole> users = dashboardRoleRepository
                .getAllUsersByDashboardId(dashboardRepository.getDashboardByHash(hash).getId());
        List<UserDto> userList = new ArrayList<>();
        for (DashboardRole user : users) {
            User user1 = userRepository.getUserById(user.getUserId());
            user1.setPassword("");
            UserDto userDto = new UserDto();
            userDto.setRoleId(user.getRoleId());
            userDto.setUser(user1);
            userList.add(userDto);
        }
        return userList;
    }
}
