package v.nevolin.repository;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import v.nevolin.model.db.Dashboard;
import v.nevolin.model.db.Log;
import v.nevolin.model.db.Task;
import v.nevolin.model.db.User;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static util.RandomString.randomString;

public class LogRepositoryImplTest {

    private static TaskRepositoryImpl taskRepository;
    private static DashboardRepositoryImpl dashboardRepository;
    private static UserRepositoryImpl userRepository;
    private static StateRepositoryImpl stateRepository;
    private static LogRepositoryImpl logRepository;

    @BeforeClass
    public static void init() {
        EmbeddedDatabase database = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2).build();
        dashboardRepository = new DashboardRepositoryImpl(new NamedParameterJdbcTemplate(database));
        userRepository = new UserRepositoryImpl(new NamedParameterJdbcTemplate(database));
        stateRepository = new StateRepositoryImpl(new NamedParameterJdbcTemplate(database));
        taskRepository = new TaskRepositoryImpl(new NamedParameterJdbcTemplate(database));
        logRepository = new LogRepositoryImpl(new NamedParameterJdbcTemplate(database));
    }

    @Test
    public void testAdd() {
        User user1 = new User();
        user1.setName("uName1");
        user1.setPassword("pass1");
        user1.setEmail("email1");
        user1 = userRepository.addUser(user1);
        User user2 = new User();
        user2.setName("uName2");
        user2.setPassword("pass2");
        user2.setEmail("email2");
        user2 = userRepository.addUser(user2);
        Dashboard dashboard = new Dashboard();
        dashboard.setName("dName1");
        dashboard.setHash(randomString());
        dashboard = dashboardRepository.addDashboard(dashboard);
        Task task = new Task();
        task.setName("tName1");
        task.setStateId(0L);
        task.setDescription("desc1");
        task.setLastUpdatedTime("last1");
        task.setCreatedTime("created1");
        task.setDashboardId(dashboard.getId());
        task.setCreatedByUserId(user1.getId());
        task.setAssignedToUserId(user2.getId());
        task = taskRepository.addTask(task);
        Log log = new Log();
        log.setAuthorUserId(user1.getId());
        log.setTaskId(task.getId());
        log.setCreatedTime("created6");
        log.setComment("comment1");
        log.setTime(30);
        log = logRepository.addLog(log);
        assertThat(log.getId(), notNullValue());
        assertThat(log.getComment(), is("comment1"));
        assertThat(log.getCreatedTime(), is("created6"));
        assertThat(taskRepository.getTaskById(log.getTaskId()).getId(), is(task.getId()));
        assertThat(log.getTime(), is(30));
        assertThat(userRepository.getUserById(log.getAuthorUserId()).getId(), is(user1.getId()));
    }
}