package v.nevolin.repository;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import v.nevolin.model.db.Dashboard;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class DashboardRepositoryImplTest {

    private static DashboardRepositoryImpl repository;

    @BeforeClass
    public static void init() {
        repository = new DashboardRepositoryImpl(new NamedParameterJdbcTemplate(new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2).build()));
    }

    @Test
    public void testAdd() {
        Dashboard dashboard1 = new Dashboard();
        dashboard1.setName("name1");
        dashboard1.setHash("hash1");
        Dashboard dashboard2 = repository.addDashboard(dashboard1);
        assertThat(dashboard2.getName(), is(dashboard1.getName()));
        assertThat(dashboard2.getHash(), is(dashboard1.getHash()));
        assertThat(dashboard2.getId(), notNullValue());
    }

    @Test
    public void testGetById() {
        Dashboard dashboard1 = new Dashboard();
        dashboard1.setName("name2");
        dashboard1.setHash("hash2");
        Dashboard dashboard2 = repository.getDashboardById(repository.addDashboard(dashboard1).getId());
        assertThat(dashboard2.getName(), is(dashboard1.getName()));
        assertThat(dashboard2.getHash(), is(dashboard1.getHash()));
        assertThat(dashboard2.getId(), notNullValue());
    }

    @Test
    public void testGetByHash() {
        Dashboard dashboard1 = new Dashboard();
        dashboard1.setName("name3");
        dashboard1.setHash("hash3");
        Dashboard dashboard2 = repository.getDashboardByHash(repository.addDashboard(dashboard1).getHash());
        assertThat(dashboard2.getName(), is(dashboard1.getName()));
        assertThat(dashboard2.getHash(), is(dashboard1.getHash()));
        assertThat(dashboard2.getId(), notNullValue());
    }
}