package v.nevolin.repository;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import v.nevolin.model.db.User;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class UserRepositoryImplTest {

    private static UserRepositoryImpl repository;

    @BeforeClass
    public static void init() {
        repository = new UserRepositoryImpl(new NamedParameterJdbcTemplate(new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2).build()));
    }

    @Test
    public void testAddUser() {
        User user1 = new User();
        user1.setEmail("ema");
        user1.setName("vova");
        user1.setPassword("121321");
        User user2 = repository.addUser(user1);
        assertThat(user1.getName(), is(user2.getName()));
        assertThat(user1.getEmail(), is(user2.getEmail()));
        assertThat(user1.getPassword(), is(user2.getPassword()));
        assertThat(user2.getId(), notNullValue());
    }

    @Test
    public void testByName() {
        User user1 = new User();
        user1.setEmail("ema");
        user1.setName("vova1");
        user1.setPassword("123521");
        repository.addUser(user1);
        User user2 = repository.getUserByName(user1.getName());
        assertThat(user1.getName(), is(user2.getName()));
        assertThat(user1.getEmail(), is(user2.getEmail()));
        assertThat(user1.getPassword(), is(user2.getPassword()));
        assertThat(user2.getId(), notNullValue());
    }

    @Test
    public void testById() {
        User user1 = new User();
        user1.setEmail("ema2");
        user1.setName("vova2");
        user1.setPassword("123231");
        repository.addUser(user1);
        User user2 = repository.getUserById(repository.getUserByName(user1.getName()).getId());
        assertThat(user1.getName(), is(user2.getName()));
        assertThat(user1.getEmail(), is(user2.getEmail()));
        assertThat(user1.getPassword(), is(user2.getPassword()));
        assertThat(user2.getId(), notNullValue());
    }

}