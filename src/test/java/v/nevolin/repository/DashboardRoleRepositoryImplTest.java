package v.nevolin.repository;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import v.nevolin.model.db.Dashboard;
import v.nevolin.model.db.DashboardRole;
import v.nevolin.model.db.User;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static util.RandomString.randomString;

public class DashboardRoleRepositoryImplTest {
    private static DashboardRoleRepositoryImpl dashboardRoleRepository;
    private static DashboardRepositoryImpl dashboardRepository;
    private static UserRepositoryImpl userRepository;
    private static RoleRepositoryImpl roleRepository;

    @BeforeClass
    public static void init() {
        EmbeddedDatabase database = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2).build();
        dashboardRepository = new DashboardRepositoryImpl(new NamedParameterJdbcTemplate(database));
        userRepository = new UserRepositoryImpl(new NamedParameterJdbcTemplate(database));
        roleRepository = new RoleRepositoryImpl(new NamedParameterJdbcTemplate(database));
        dashboardRoleRepository = new DashboardRoleRepositoryImpl(new NamedParameterJdbcTemplate(database));
    }

    @Test
    public void testAdd() {
        User user = new User();
        user.setName("uName1");
        user.setPassword("pass1");
        user.setEmail("email1");
        user = userRepository.addUser(user);
        Dashboard dashboard = new Dashboard();
        dashboard.setName("dName1");
        dashboard.setHash(randomString());
        dashboard = dashboardRepository.addDashboard(dashboard);
        DashboardRole dashboardRole = new DashboardRole();
        dashboardRole.setDashboardId(dashboard.getId());
        dashboardRole.setRoleId(0L);
        dashboardRole.setUserId(user.getId());
        dashboardRole = dashboardRoleRepository.addUserToDashboard(dashboardRole);
        assertThat(dashboardRole.getDashboardId(), is(dashboard.getId()));
        assertThat(dashboardRole.getRoleId(), is(0L));
        assertThat(dashboardRole.getUserId(), is(user.getId()));
    }

    @Test
    public void testDelete() {
        User user = new User();
        user.setName("uName2");
        user.setPassword("pass2");
        user.setEmail("email2");
        user = userRepository.addUser(user);
        Dashboard dashboard = new Dashboard();
        dashboard.setName("dName2");
        dashboard.setHash(randomString());
        dashboard = dashboardRepository.addDashboard(dashboard);
        DashboardRole dashboardRole = new DashboardRole();
        dashboardRole.setDashboardId(dashboard.getId());
        dashboardRole.setRoleId(0L);
        dashboardRole.setUserId(user.getId());
        dashboardRole = dashboardRoleRepository.addUserToDashboard(dashboardRole);
        int res = dashboardRoleRepository.deleteUserFromDashboard(dashboardRole.getDashboardId(),
                dashboardRole.getUserId());
        assertThat(res, is(1));
        assertThat(dashboardRoleRepository.getAllUsersByDashboardId(dashboardRole.getDashboardId()).size(), is(0));
    }

    @Test
    public void testChangeRole() {
        User user = new User();
        user.setName("uName3");
        user.setPassword("pass3");
        user.setEmail("email3");
        user = userRepository.addUser(user);
        Dashboard dashboard = new Dashboard();
        dashboard.setName("dName3");
        dashboard.setHash(randomString());
        dashboard = dashboardRepository.addDashboard(dashboard);
        DashboardRole dashboardRole = new DashboardRole();
        dashboardRole.setDashboardId(dashboard.getId());
        dashboardRole.setRoleId(0L);
        dashboardRole.setUserId(user.getId());
        dashboardRole = dashboardRoleRepository.addUserToDashboard(dashboardRole);
        dashboardRole.setRoleId(1L);
        dashboardRole = dashboardRoleRepository.changeUserRole(dashboardRole);
        assertThat(dashboardRole.getDashboardId(), is(dashboard.getId()));
        assertThat(dashboardRole.getRoleId(), is(1L));
        assertThat(dashboardRole.getUserId(), is(user.getId()));
    }

    @Test
    public void testDashboards() {
        User user = new User();
        user.setName("uName3");
        user.setPassword("pass3");
        user.setEmail("email3");
        user = userRepository.addUser(user);

        Dashboard dashboard1 = new Dashboard();
        dashboard1.setName("dName4");
        dashboard1.setHash(randomString());
        dashboard1 = dashboardRepository.addDashboard(dashboard1);
        Dashboard dashboard2 = new Dashboard();
        dashboard2.setName("dName5");
        dashboard2.setHash(randomString());
        dashboard2 = dashboardRepository.addDashboard(dashboard2);

        DashboardRole dashboardRole1 = new DashboardRole();
        dashboardRole1.setDashboardId(dashboard1.getId());
        dashboardRole1.setRoleId(0L);
        dashboardRole1.setUserId(user.getId());
        dashboardRole1 = dashboardRoleRepository.addUserToDashboard(dashboardRole1);

        DashboardRole dashboardRole2 = new DashboardRole();
        dashboardRole2.setDashboardId(dashboard2.getId());
        dashboardRole2.setRoleId(0L);
        dashboardRole2.setUserId(user.getId());
        dashboardRole2 = dashboardRoleRepository.addUserToDashboard(dashboardRole2);

        assertThat(dashboardRoleRepository.getAllUsersDashboards(user.getId()), containsInAnyOrder(dashboardRole1, dashboardRole2));
    }
}