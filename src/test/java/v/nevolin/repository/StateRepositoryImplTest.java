package v.nevolin.repository;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import v.nevolin.model.db.State;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class StateRepositoryImplTest {
    private static StateRepositoryImpl repository;

    @BeforeClass
    public static void init() {
        repository = new StateRepositoryImpl(new NamedParameterJdbcTemplate(new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2).build()));
    }

    @Test
    public void testGetById() {
        State state = repository.getStateById(0L);
        assertThat(state.getId(), is(0L));
        assertThat(state.getName(), is("To Do"));
        state = repository.getStateById(1L);
        assertThat(state.getId(), is(1L));
        assertThat(state.getName(), is("In Progress"));
        state = repository.getStateById(2L);
        assertThat(state.getId(), is(2L));
        assertThat(state.getName(), is("Done"));
    }
}