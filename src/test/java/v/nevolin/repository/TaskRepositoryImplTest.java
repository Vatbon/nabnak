package v.nevolin.repository;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import v.nevolin.model.db.Dashboard;
import v.nevolin.model.db.Task;
import v.nevolin.model.db.User;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static util.RandomString.randomString;

public class TaskRepositoryImplTest {

    private static TaskRepositoryImpl taskRepository;
    private static DashboardRepositoryImpl dashboardRepository;
    private static UserRepositoryImpl userRepository;
    private static StateRepositoryImpl stateRepository;

    @BeforeClass
    public static void init() {
        EmbeddedDatabase database = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2).build();
        dashboardRepository = new DashboardRepositoryImpl(new NamedParameterJdbcTemplate(database));
        userRepository = new UserRepositoryImpl(new NamedParameterJdbcTemplate(database));
        stateRepository = new StateRepositoryImpl(new NamedParameterJdbcTemplate(database));
        taskRepository = new TaskRepositoryImpl(new NamedParameterJdbcTemplate(database));
    }

    @Test
    public void testAdd() {
        User user1 = new User();
        user1.setName("uName1");
        user1.setPassword("pass1");
        user1.setEmail("email1");
        user1 = userRepository.addUser(user1);
        User user2 = new User();
        user2.setName("uName2");
        user2.setPassword("pass2");
        user2.setEmail("email2");
        user2 = userRepository.addUser(user2);
        Dashboard dashboard = new Dashboard();
        dashboard.setName("dName1");
        dashboard.setHash(randomString());
        dashboard = dashboardRepository.addDashboard(dashboard);
        Task task = new Task();
        task.setName("tName1");
        task.setStateId(0L);
        task.setDescription("desc1");
        task.setLastUpdatedTime("last1");
        task.setCreatedTime("created1");
        task.setDashboardId(dashboard.getId());
        task.setCreatedByUserId(user1.getId());
        task.setAssignedToUserId(user2.getId());
        task = taskRepository.addTask(task);
        assertThat(task.getId(), notNullValue());
        assertThat(task.getName(), is("tName1"));
        assertThat(task.getStateId(), is(0L));
        assertThat(task.getDescription(), is("desc1"));
        assertThat(task.getCreatedTime(), is("created1"));
        assertThat(task.getCreatedByUserId(), is(user1.getId()));
        assertThat(task.getAssignedToUserId(), is(user2.getId()));
        assertThat(userRepository.getUserById(task.getCreatedByUserId()).getId(), is(user1.getId()));
        assertThat(userRepository.getUserById(task.getAssignedToUserId()).getId(), is(user2.getId()));
        assertThat(dashboardRepository.getDashboardById(task.getDashboardId()).getHash(), is(dashboard.getHash()));
    }

    @Test
    public void testGetByDashboardId() {
        User user1 = new User();
        user1.setName("uName3");
        user1.setPassword("pass3");
        user1.setEmail("email3");
        user1 = userRepository.addUser(user1);
        User user2 = new User();
        user2.setName("uName4");
        user2.setPassword("pass4");
        user2.setEmail("email4");
        user2 = userRepository.addUser(user2);
        Dashboard dashboard = new Dashboard();
        dashboard.setName("dName2");
        dashboard.setHash(randomString());
        dashboard = dashboardRepository.addDashboard(dashboard);
        Task task1 = new Task();
        task1.setName("tName2");
        task1.setDescription("desc2");
        task1.setLastUpdatedTime("last2");
        task1.setCreatedTime("created2");
        task1.setDashboardId(dashboard.getId());
        task1.setCreatedByUserId(user1.getId());
        task1.setAssignedToUserId(user2.getId());
        task1 = taskRepository.addTask(task1);
        Task task2 = new Task();
        task2.setName("tName3");
        task2.setDescription("desc3");
        task2.setLastUpdatedTime("last3");
        task2.setCreatedTime("created3");
        task2.setDashboardId(dashboard.getId());
        task2.setCreatedByUserId(user1.getId());
        task2.setAssignedToUserId(user2.getId());
        task2 = taskRepository.addTask(task2);
        List<Task> taskList = taskRepository.getTasksByDashboardId(dashboard.getId());
        assertThat(taskList, containsInAnyOrder(task1, task2));
    }

    @Test
    public void testUpdate() {
        User user1 = new User();
        user1.setName("uName4");
        user1.setPassword("pass4");
        user1.setEmail("email4");
        user1 = userRepository.addUser(user1);
        User user2 = new User();
        user2.setName("uName5");
        user2.setPassword("pass5");
        user2.setEmail("email5");
        user2 = userRepository.addUser(user2);
        Dashboard dashboard = new Dashboard();
        dashboard.setName("dName3");
        dashboard.setHash(randomString());
        dashboard = dashboardRepository.addDashboard(dashboard);
        Dashboard dashboard1 = new Dashboard();
        dashboard1.setName("dName4");

        dashboard1.setHash(randomString());
        dashboard1 = dashboardRepository.addDashboard(dashboard1);
        Task task1 = new Task();
        task1.setName("tName3");
        task1.setStateId(0L);
        task1.setDescription("desc3");
        task1.setLastUpdatedTime("last3");
        task1.setCreatedTime("created3");
        task1.setDashboardId(dashboard.getId());
        task1.setCreatedByUserId(user1.getId());
        task1.setAssignedToUserId(user2.getId());
        task1 = taskRepository.addTask(task1);
        task1.setName("tName4");
        task1.setStateId(1L);
        task1.setDescription("desc4");
        task1.setLastUpdatedTime("last4");
        task1.setCreatedTime("created4");
        task1.setDashboardId(dashboard1.getId());
        task1.setCreatedByUserId(user2.getId());
        task1.setAssignedToUserId(user1.getId());
        task1 = taskRepository.updateTask(task1);
        assertThat(task1.getId(), notNullValue());
        assertThat(task1.getName(), is("tName4"));
        assertThat(task1.getStateId(), is(1L));
        assertThat(task1.getDescription(), is("desc4"));
        assertThat(task1.getCreatedTime(), is("created3"));
        assertThat(task1.getCreatedByUserId(), is(user1.getId()));
        assertThat(task1.getAssignedToUserId(), is(user1.getId()));
        assertThat(userRepository.getUserById(task1.getCreatedByUserId()).getId(), is(user1.getId()));
        assertThat(userRepository.getUserById(task1.getAssignedToUserId()).getId(), is(user1.getId()));
        assertThat(dashboardRepository.getDashboardById(task1.getDashboardId()).getHash(), is(dashboard.getHash()));
    }
}