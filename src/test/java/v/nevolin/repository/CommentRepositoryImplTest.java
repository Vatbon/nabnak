package v.nevolin.repository;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import v.nevolin.model.db.Comment;
import v.nevolin.model.db.Dashboard;
import v.nevolin.model.db.Task;
import v.nevolin.model.db.User;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static util.RandomString.randomString;

public class CommentRepositoryImplTest {

    private static TaskRepositoryImpl taskRepository;
    private static DashboardRepositoryImpl dashboardRepository;
    private static UserRepositoryImpl userRepository;
    private static StateRepositoryImpl stateRepository;
    private static CommentRepositoryImpl commentRepository;

    @BeforeClass
    public static void init() {
        EmbeddedDatabase database = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2).build();
        dashboardRepository = new DashboardRepositoryImpl(new NamedParameterJdbcTemplate(database));
        userRepository = new UserRepositoryImpl(new NamedParameterJdbcTemplate(database));
        stateRepository = new StateRepositoryImpl(new NamedParameterJdbcTemplate(database));
        taskRepository = new TaskRepositoryImpl(new NamedParameterJdbcTemplate(database));
        commentRepository = new CommentRepositoryImpl(new NamedParameterJdbcTemplate(database));
    }

    @Test
    public void testAdd() {
        User user1 = new User();
        user1.setName("uName1");
        user1.setPassword("pass1");
        user1.setEmail("email1");
        user1 = userRepository.addUser(user1);
        User user2 = new User();
        user2.setName("uName2");
        user2.setPassword("pass2");
        user2.setEmail("email2");
        user2 = userRepository.addUser(user2);
        Dashboard dashboard = new Dashboard();
        dashboard.setName("dName1");
        dashboard.setHash(randomString());
        dashboard = dashboardRepository.addDashboard(dashboard);
        Task task = new Task();
        task.setName("tName1");
        task.setStateId(0L);
        task.setDescription("desc1");
        task.setLastUpdatedTime("last1");
        task.setCreatedTime("created1");
        task.setDashboardId(dashboard.getId());
        task.setCreatedByUserId(user1.getId());
        task.setAssignedToUserId(user2.getId());
        task = taskRepository.addTask(task);
        Comment comment = new Comment();
        comment.setTaskId(task.getId());
        comment.setAuthorUserId(user1.getId());
        comment.setCreatedTime("created6");
        comment.setBody("comment1");
        comment = commentRepository.addComment(comment);
        assertThat(comment.getId(), notNullValue());
        assertThat(comment.getBody(), is("comment1"));
        assertThat(comment.getCreatedTime(), is("created6"));
        assertThat(taskRepository.getTaskById(comment.getTaskId()).getId(), is(task.getId()));
        assertThat(userRepository.getUserById(comment.getAuthorUserId()).getId(), is(user1.getId()));
    }
}