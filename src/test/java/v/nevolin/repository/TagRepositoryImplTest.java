package v.nevolin.repository;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import v.nevolin.model.db.Dashboard;
import v.nevolin.model.db.Tag;
import v.nevolin.model.db.Task;
import v.nevolin.model.db.User;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static util.RandomString.randomString;

public class TagRepositoryImplTest {
    private static TaskRepositoryImpl taskRepository;
    private static DashboardRepositoryImpl dashboardRepository;
    private static UserRepositoryImpl userRepository;
    private static StateRepositoryImpl stateRepository;
    private static TagRepositoryImpl tagRepository;

    @BeforeClass
    public static void init() {
        EmbeddedDatabase database = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2).build();
        dashboardRepository = new DashboardRepositoryImpl(new NamedParameterJdbcTemplate(database));
        userRepository = new UserRepositoryImpl(new NamedParameterJdbcTemplate(database));
        stateRepository = new StateRepositoryImpl(new NamedParameterJdbcTemplate(database));
        taskRepository = new TaskRepositoryImpl(new NamedParameterJdbcTemplate(database));
        tagRepository = new TagRepositoryImpl(new NamedParameterJdbcTemplate(database));
    }

    @Test
    public void testAdd() {
        User user1 = new User();
        user1.setName("uName1");
        user1.setPassword("pass1");
        user1.setEmail("email1");
        user1 = userRepository.addUser(user1);
        User user2 = new User();
        user2.setName("uName2");
        user2.setPassword("pass2");
        user2.setEmail("email2");
        user2 = userRepository.addUser(user2);
        Dashboard dashboard = new Dashboard();
        dashboard.setName("dName1");
        dashboard.setHash(randomString());
        dashboard = dashboardRepository.addDashboard(dashboard);
        Task task = new Task();
        task.setName("tName1");
        task.setStateId(0L);
        task.setDescription("desc1");
        task.setLastUpdatedTime("last1");
        task.setCreatedTime("created1");
        task.setDashboardId(dashboard.getId());
        task.setCreatedByUserId(user1.getId());
        task.setAssignedToUserId(user2.getId());
        task = taskRepository.addTask(task);
        Tag tag = new Tag();
        tag.setName("tag1");
        tag.setTaskId(task.getId());
        tag = tagRepository.addTag(tag);
        assertThat(taskRepository.getTaskById(tag.getTaskId()).getId(), is(task.getId()));
    }

    @Test
    public void testDelete() {
        User user1 = new User();
        user1.setName("uName1");
        user1.setPassword("pass1");
        user1.setEmail("email1");
        user1 = userRepository.addUser(user1);
        User user2 = new User();
        user2.setName("uName2");
        user2.setPassword("pass2");
        user2.setEmail("email2");
        user2 = userRepository.addUser(user2);
        Dashboard dashboard = new Dashboard();
        dashboard.setName("dName1");
        dashboard.setHash(randomString());
        dashboard = dashboardRepository.addDashboard(dashboard);
        Task task = new Task();
        task.setName("tName1");
        task.setStateId(0L);
        task.setDescription("desc1");
        task.setLastUpdatedTime("last1");
        task.setCreatedTime("created1");
        task.setDashboardId(dashboard.getId());
        task.setCreatedByUserId(user1.getId());
        task.setAssignedToUserId(user2.getId());
        task = taskRepository.addTask(task);
        Tag tag = new Tag();
        tag.setName("tag1");
        tag.setTaskId(task.getId());
        tag = tagRepository.addTag(tag);
        int res = tagRepository.deleteTag(tag);
        assertThat(res, is(1));
        assertThat(tagRepository.getTaskTags(task.getId()), empty());
    }

    @Test
    public void testLst() {
        User user1 = new User();
        user1.setName("uName1");
        user1.setPassword("pass1");
        user1.setEmail("email1");
        user1 = userRepository.addUser(user1);
        User user2 = new User();
        user2.setName("uName2");
        user2.setPassword("pass2");
        user2.setEmail("email2");
        user2 = userRepository.addUser(user2);
        Dashboard dashboard = new Dashboard();
        dashboard.setName("dName1");
        dashboard.setHash(randomString());
        dashboard = dashboardRepository.addDashboard(dashboard);
        Task task = new Task();
        task.setName("tName1");
        task.setStateId(0L);
        task.setDescription("desc1");
        task.setLastUpdatedTime("last1");
        task.setCreatedTime("created1");
        task.setDashboardId(dashboard.getId());
        task.setCreatedByUserId(user1.getId());
        task.setAssignedToUserId(user2.getId());
        task = taskRepository.addTask(task);
        Tag tag = new Tag();
        tag.setName("tag1");
        tag.setTaskId(task.getId());
        tag = tagRepository.addTag(tag);
        Tag tag1 = new Tag();
        tag1.setName("tag2");
        tag1.setTaskId(task.getId());
        tag1 = tagRepository.addTag(tag1);
        assertThat(tagRepository.getTaskTags(task.getId()), containsInAnyOrder(tag, tag1));
    }
}