package v.nevolin.repository;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import v.nevolin.model.db.Role;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class RoleRepositoryImplTest {
    private static RoleRepositoryImpl repository;

    @BeforeClass
    public static void init() {
        repository = new RoleRepositoryImpl(new NamedParameterJdbcTemplate(new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2).build()));
    }

    @Test
    public void testInit() {
        Role role = repository.getRoleById(0L);
        assertThat(role.getId(), is(0L));
        assertThat(role.getName(), is("Admin"));
        role = repository.getRoleById(1L);
        assertThat(role.getId(), is(1L));
        assertThat(role.getName(), is("Participant"));
        role = repository.getRoleById(2L);
        assertThat(role.getId(), is(2L));
        assertThat(role.getName(), is("Guest"));
    }
}