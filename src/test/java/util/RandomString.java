package util;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomString {
    private static final List<String> strings = new ArrayList<>();

    public static String randomString() {
        Random random = new Random();
        byte[] bytes = new byte[100];
        String result = "";
        do {
            random.nextBytes(bytes);
            result = new String(bytes, StandardCharsets.UTF_8);
        } while(strings.contains(result));
        strings.add(result);
        return result;
    }
}
